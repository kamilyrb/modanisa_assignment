import { shallowMount } from '@vue/test-utils'
import TodoItem from '@/components/TodoItem'


describe('TodoItem Layout', () => {

    it('render a card div', () => {
      const wrapper = shallowMount(TodoItem);
      const el = wrapper.findAll('[data-testid="todo-item-div"]')
      expect(el.length).toBe(1)
    })

    it('rdisplay prop text inside component', () => {
        const wrapper = shallowMount(TodoItem,
          {
            props: {
              text: "first sample todo text"
            }
          });
        expect(wrapper.text()).toContain("first sample todo text");
      })
  
  

})
