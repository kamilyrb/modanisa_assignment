import axios from "axios";

export const API_URL = "http://todo-backend.kamilyrb.com"

export const getTodos = (baseUrl) => {
    let url = "/api/todos/"
    if (baseUrl) {
        url = baseUrl + url
    } else {
        url = API_URL + url
    }
    const resp = axios.get(url);
    return resp
};

export const addTodo = (todoText, baseUrl) => {
    let url = "/api/todos/"
    if (baseUrl) {
        url = baseUrl + url
    } else {
        url = API_URL + url
    }
    const resp = axios.post(url, {
        text: todoText
    });
    return resp
};