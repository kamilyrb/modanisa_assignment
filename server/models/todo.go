package models

type Todo struct {
	ID   uint   `gorm:"primary_key" json:"id"`
	Text string `json:"text"`
}
