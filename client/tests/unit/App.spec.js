import App from "@/App"
import TodoList from "@/components/TodoList"
import AddTodoItem from "@/components/AddTodoItem"


import { mount } from "@vue/test-utils";
import {setupServer} from "msw/node"
import { rest } from "msw"
import { render, screen } from "@testing-library/vue";
import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";
import {API_URL} from "@/api/apiCalls"

describe('App Layout', () => {
    const todos = [
        {
            id: 1,
            text: "Todo item 1",
        },
        {
            id: 2,
            text: "Todo item 2",
        },
    ]

    const server = setupServer(
        rest.get(API_URL + "/api/todos/", (req, res, ctx) => {
            return  res(ctx.status(200), ctx.json(todos));
        })
    )

    beforeEach(()=>{
        server.listen()
    })

    afterEach(()=>{
        server.close()

    })

    it('render TodoList component correctly', () => {
      const wrapper = mount(App);

      const todoListComponent = wrapper.findComponent(TodoList)
      expect(todoListComponent.exists()).toBe(true)
    })

    it('render Todo items component correctly', async () => {
        render(App);
        const todosItems = await screen.findAllByTestId("todo-item-div")
        expect(todosItems.length).toBe(2)

      })

      it('render AddTodoItem component correctly', async () => {
        const wrapper = mount(App);

        const todoListComponent = wrapper.findComponent(AddTodoItem)
        expect(todoListComponent.exists()).toBe(true)

      })

      it("display added todo item on list", async () => {
        render(App);
        const todoText = "Todo item iwth id 3"
        server.use(
            rest.post(API_URL + "/api/todos/", (req, res, ctx) => {
                return res(ctx.status(201), ctx.json({
                    id: 3,
                    text: todoText
                }));
            })
        );

        const todoTextInput = screen.queryByPlaceholderText("Todo text");
        const button = screen.queryByRole("button", { name: "Add" });
    
        expect(button).toBeDisabled();
        await userEvent.type(todoTextInput, todoText);

        await userEvent.click(button);
        const todosItems = await screen.findAllByTestId("todo-item-div")
        expect(todosItems.length).toBe(2)
        
    });


})
