package routers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/kamilyrb/modanisa_assignment/server/handler"
)

func SetupRoutes(app *fiber.App) {
	app.Get("/", handler.GetHome)
	api := app.Group("/api")
	api.Get("/todos/", handler.GetAllTodos)
	api.Post("/todos/", handler.CreateTodo)
}
