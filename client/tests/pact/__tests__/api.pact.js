/**
 * @jest-environment node
 */
 import { pactWith } from 'jest-pact';
 import { getTodos } from "../../../src/api/apiCalls"
  
 
 pactWith({
     consumer: "todoclient", provider: "todoserver"
 }, provider => {
     describe("ToDo API", () => {
         const TODOS_DATA = [
             {
                 id: 1,
                 text: "Todo Item 1"
             }
         ]
         const todosSuccessResponse = {
             status: 200,
             headers: {
                 "Content-Type": "application/json",
             },
             body: TODOS_DATA,
         }
         const todosListRequest = {
             uponReceiving: "a request for todos",
             withRequest: {
                 method: "GET",
                 path: "/api/todos/",
                 headers: {
                     "Accept": "application/json, text/plain, */*",
                 },
             },
         }
 
         beforeEach(() => {
             const interaction = {
                 state: "i have a list of todos",
                 ...todosListRequest,
                 willRespondWith: todosSuccessResponse,
             }
             return provider.addInteraction(interaction)
         })
 
         it("returns a successful body", async () => {
             const result = await getTodos(provider.mockService.baseUrl)
             expect(result.data).toEqual(TODOS_DATA)
         })

         afterEach(()=>{provider.verify()})
 
     })
 
 
 })

 

 
     