import { mount, shallowMount } from '@vue/test-utils'
import TodoList from '@/components/TodoList'
import TodoItem from '@/components/TodoItem'


describe('TodoList Layout', () => {

    it('render component and display title correctly', () => {
      const wrapper = shallowMount(TodoList);

      const div = wrapper.find('h4')
      expect(div.exists()).toBe(true)
      expect(div.text()).toEqual('Todo Items')

    })

    it('display todo items correctly', () => {
        const wrapper = mount(TodoList,
            {
                props: {
                    todos: [
                        {
                            id: 1,
                            text: "Todo item 1",
                        },
                        {
                            id: 2,
                            text: "Todo item 2",
                        },
                    ],
                }

            });

        const todoItemComponents = wrapper.findAllComponents(TodoItem)
        expect(todoItemComponents.length).toBe(2)
        expect(wrapper.text()).toContain("Todo item 1")
        expect(wrapper.text()).toContain("Todo item 2")

    });


})
