# About The Project
This is a simple project developed for modanisa technologist position assignment.
There are basically two interface functions. View todo list and add new todo.

## Tech Stack

- Golang
- Vue.js
- Kubernetes
- Helm
- Postgresql
- Pact
- Docker


## Architecture:

### Backend
The backend side was developed with the golang [fiber](https://github.com/gofiber/fiber) framework. The [gorm](https://github.com/go-gorm/gorm) orm tool
was used for database connection. [Testify](https://github.com/stretchr/testify) library was used for backend tests. 
There are basically 3 endpoints.
 - `/` -> `GET` returns 200 status code to say to ingress it's healthy
 - `/api/todos/` -> `GET` returns 200 status code with todos list that retrieved from db.
 - `/api/todos/` -> `POST` accepts the todo to be added and returns 201 status code.

### Frontend
Frontend application is basically an application written in [vue.js](https://github.com/vuejs/vue), where the todo list can be viewed and new todo records 
can be added. [jest](https://jestjs.io/) library was used for application tests. In addition, 
[mocks service worker](https://mswjs.io/) was used to mock the api requests sent in the backend. 
In addition, [testing library](https://testing-library.com/) was used to imitate user events (pressing the button, 
entering text on the input). Application interface is accessible at http://todo-frontend.kamilyrb.com/

### Postgresql
Postgresql was used as database. Here it simply has a single table called todo, 
with id and text columns.

### Testing
Backend and frontend applications have separate unit tests. In addition, tests were written for the pact tool, which 
is used as a cdc integration test tool, in backend and frontend applications.

### Pact Broker
In order for the backend and frontend applications to shake hands as a result of 
the pact tests, a pact broker was installed on a separate server on the google cloud.
After the relevant validation works are done, this broker is asked whether 
a version can be released in the pipeline. The broker can be accessed at 
[http://pact-broker.kamilyrb.com/](In order for the backend and frontend applications to shake hands as a result of the pact tests, a pact broker was installed on a separate server on the google cloud. After the relevant validation works are done, this broker is asked whether a version can be released in the pipeline. The broker can be accessed at http://pact-broker.kamilyrb.com/. The pactbroker value can be used as the username and password.). The `pactbroker` value can be used as the 
username and password.

## Deployment
The deployment process is automated with each commit. As a result of the pipeline, deployment is made to the google 
cloud kubernetes cluster with helm charts.


## Pipeline
There is a pipeline in the project that is triggered by each commit, from testing to deploying the application. 
If we want to examine this pipeline step by step:

#### 1-) Test Frontend
In this step, the tests of the frontend application are run. As a result of the 
tests, pact tests are also run and the generated json file is sent to the pact 
broker.


#### 2-) Test Backend
In this step, the tests of the backend application are run. As a result of the tests, additional pact tests are 
run and the json file that was previously sent to the pact broker is validated.

#### 3-) Build Backend
The docker image of the backend application is built and sent to dockerhub.


#### 4-) Build Frontend
The docker image of the frontend application is built and sent to dockerhub.

#### 5-) Can I Deploy
n this step, the pact broker is asked whether the version can be released 
as a result of the test.


#### 6-) Deploy Postgresql
In this step, the database is deployed

#### 7-) Deploy Backend
In this step, the backend is deployed


#### 8-) Deploy Postgresql
In this step, the frontend is deployed

