import os
import json
import urllib.request

version_number = os.environ['VERSION_NUMBER']
pact_url = os.environ['PACT_URL']
basic_auth_token = os.environ['PACT_TOKEN']

url = f'{pact_url}can-i-deploy?pacticipant=todoserver&version={version_number}&to=prod'
headers = {
    'Authorization': f'Basic {basic_auth_token}'
}

req = urllib.request.Request(url, headers=headers)
response = urllib.request.urlopen(req)
result = json.loads(response.read().decode('utf8'))
try:
    if result['summary']['deployable'] != True:
        exit(1)
except Exception:
    exit(1)
exit(0)
