import { shallowMount } from '@vue/test-utils'
import AddTodoItem from '@/components/AddTodoItem'
import { render, screen } from "@testing-library/vue";
import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";


describe('AddTodoItem Layout', () => {

  it('render button and input', () => {
    const wrapper = shallowMount(AddTodoItem);

    const todoInput = wrapper.find('input');
    expect(todoInput.exists()).toBe(true);

    const addButton = wrapper.find('button')
    expect(addButton.exists()).toBe(true)

    const isDisabled = addButton.element.disabled === true;
    expect(isDisabled).toBe(true)
  })

  it("enables the button when input has value", async () => {
    render(AddTodoItem);
    const todoTextInput = screen.queryByPlaceholderText("Todo text");
    const button = screen.queryByRole("button", { name: "Add" });

    expect(button).toBeDisabled();
    await userEvent.type(todoTextInput, "todo sample");
    expect(button).toBeEnabled();
  });

})