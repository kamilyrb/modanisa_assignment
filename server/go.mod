module gitlab.com/kamilyrb/modanisa_assignment/server

go 1.16

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/gofiber/fiber/v2 v2.20.2
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/pact-foundation/pact-go v1.6.4
	github.com/stretchr/testify v1.7.0
	github.com/valyala/fasthttp v1.31.0 // indirect
	golang.org/x/sys v0.0.0-20211020174200-9d6173849985 // indirect
	gorm.io/driver/postgres v1.1.2
	gorm.io/driver/sqlite v1.1.6
	gorm.io/gorm v1.21.16
)
