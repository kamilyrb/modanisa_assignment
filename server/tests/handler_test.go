package tests

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/kamilyrb/modanisa_assignment/server/database"
	"gitlab.com/kamilyrb/modanisa_assignment/server/models"
	"gitlab.com/kamilyrb/modanisa_assignment/server/routers"
	"gorm.io/gorm"
	"io/ioutil"
	"net/http"
	"reflect"
	"testing"
)

type TestSuiteEnv struct {
	suite.Suite
	db *gorm.DB
}

func (suite *TestSuiteEnv) SetupSuite() {
	database.SetupTestDatabase()
	suite.db = database.DB
}

func (suite *TestSuiteEnv) TearDownTest() {
	database.ClearTable("todos")
}

// This gets run automatically by `go test` so we call `suite.Run` inside it
func TestSuite(t *testing.T) {
	// This is what actually runs our suite
	suite.Run(t, new(TestSuiteEnv))
}

func (suite *TestSuiteEnv) Test_GetTodos_EmptyResult() {
	app := fiber.New()
	app.Use(cors.New())
	routers.SetupRoutes(app)
	t := suite.T()

	req, err := http.NewRequest(http.MethodGet, "/api/todos/", nil)
	if err != nil {
		t.Error(err)
	}
	resp, _ := app.Test(req)

	var expectedResponse []byte

	assert.Equal(t, resp.StatusCode, fiber.StatusOK)
	body, _ := ioutil.ReadAll(resp.Body)
	assert.Equal(suite.T(), reflect.TypeOf(body), reflect.TypeOf(expectedResponse))

}

func (suite *TestSuiteEnv) Test_GetTodos() {
	app := fiber.New()
	app.Use(cors.New())
	routers.SetupRoutes(app)
	t := suite.T()

	var todos = []models.Todo{{Text: "todo 1"}, {Text: "todo 2"}}
	suite.db.Create(&todos)

	req, err := http.NewRequest(http.MethodGet, "/api/todos/", nil)
	if err != nil {
		t.Error(err)
	}
	resp, _ := app.Test(req)

	var actual []models.Todo
	assert.Equal(t, resp.StatusCode, fiber.StatusOK)
	body, _ := ioutil.ReadAll(resp.Body)

	if err := json.Unmarshal(body, &actual); err != nil {
		t.Error(err)
	}
	assert.Equal(suite.T(), todos, actual)
}

func (suite *TestSuiteEnv) Test_CreateTodo() {
	type createRequestBody struct {
		text string
	}

	rb := &createRequestBody{text: "Test todo"}
	rbJson, _ := json.Marshal(rb)

	app := fiber.New()
	app.Use(cors.New())
	routers.SetupRoutes(app)
	t := suite.T()

	req, err := http.NewRequest(http.MethodPost, "/api/todos/", bytes.NewBuffer(rbJson))
	if err != nil {
		t.Error(err)
	}
	resp, _ := app.Test(req)

	assert.Equal(suite.T(), resp.StatusCode, fiber.StatusCreated)
	todo := models.Todo{}
	actual := models.Todo{}
	suite.db.First(&todo)

	body, _ := ioutil.ReadAll(resp.Body)

	if err := json.Unmarshal(body, &actual); err != nil {
		t.Error(err)
	}

	assert.Equal(suite.T(), todo, actual)

}
