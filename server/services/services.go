package services

import (
	"gitlab.com/kamilyrb/modanisa_assignment/server/database"
	"gitlab.com/kamilyrb/modanisa_assignment/server/models"
)

func GetAllTodos(todo *[]models.Todo) (err error) {
	db := database.DB
	if err = db.Find(todo).Error; err != nil {
		return err
	}
	return nil
}

func CreateTodo(todo *models.Todo) (err error) {
	db := database.DB
	if err = db.Create(todo).Error; err != nil {
		return err
	}
	return nil
}
