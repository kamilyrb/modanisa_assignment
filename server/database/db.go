package database

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/kamilyrb/modanisa_assignment/server/models"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var DB *gorm.DB

func getEnv(key, defaultValue string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return defaultValue
}
func Setup() {
	host := getEnv("DB_HOST", "localhost")
	port := getEnv("DB_PORT", "5432")
	dbname := getEnv("DB_NAME", "todoapp")
	user := getEnv("DB_USER", "postgres")
	password := getEnv("DB_PASSWORD", "password")

	dsn := "host=" + host + " port=" + port + " user=" + user + " dbname=" + dbname + " sslmode=disable password=" + password

	db, err := gorm.Open(postgres.Open(dsn))
	if err != nil {
		log.Fatal(err)
	}
	db.AutoMigrate([]models.Todo{})
	DB = db
}

func SetupTestDatabase() {
	db, err := gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	db.AutoMigrate([]models.Todo{})
	DB = db
	ClearTable("todos")
}

func ClearTable(tableName string) {
	DB.Exec(fmt.Sprintf("DELETE FROM %s", tableName))
}
