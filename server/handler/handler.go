package handler

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/kamilyrb/modanisa_assignment/server/models"
	"gitlab.com/kamilyrb/modanisa_assignment/server/services"
)

func GetHome(c *fiber.Ctx) error {
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"message": "ok",
	})
}

func GetAllTodos(c *fiber.Ctx) error {
	var todos []models.Todo
	err := services.GetAllTodos(&todos)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": "Something went wrong",
			"error":   err.Error(),
		})
	} else {
		return c.Status(fiber.StatusOK).JSON(todos)
	}
}

func CreateTodo(c *fiber.Ctx) error {
	type Request struct {
		Text string `json:"text"`
	}

	var body Request
	err := c.BodyParser(&body)

	todo := &models.Todo{
		Text: body.Text,
	}

	err = services.CreateTodo(todo)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": "Something went wrong",
			"error":   err.Error(),
		})
	} else {
		return c.Status(fiber.StatusCreated).JSON(todo)
	}
}
