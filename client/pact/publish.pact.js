const pact = require("@pact-foundation/pact-node");
const path = require("path");
require("dotenv").config({ path: __dirname + `/../.env` });


const opts = {
    pactFilesOrDirs: [path.resolve(__dirname, './pacts/todoclient-todoserver.json')],
    pactBroker: process.env.VUE_APP_PACT_BROKER_URL,
    pactBrokerUsername: process.env.VUE_APP_PACT_BROKER_USERNAME,
    pactBrokerPassword: process.env.VUE_APP_PACT_BROKER_PASSWORD,
    tags: ['prod', 'test'],
    consumerVersion:  process.env.VUE_APP_PACT_VERSION
}

pact.publishPacts(opts)
    .then(() => {
        console.log('Pact contract publishing complete!')
    })
    .catch(e => {
        console.log('Pact contract publishing failed: ', e)
    })

 