package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"gitlab.com/kamilyrb/modanisa_assignment/server/database"
	"gitlab.com/kamilyrb/modanisa_assignment/server/routers"
)

func main() {
	database.Setup()
	app := fiber.New()
	app.Use(cors.New())
	app.Use(logger.New())
	routers.SetupRoutes(app)

	app.Listen(":3000")
}
