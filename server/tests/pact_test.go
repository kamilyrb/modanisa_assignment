package tests

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"gitlab.com/kamilyrb/modanisa_assignment/server/database"
	"gitlab.com/kamilyrb/modanisa_assignment/server/models"
	"gitlab.com/kamilyrb/modanisa_assignment/server/routers"
	"log"
	"os"
	"testing"
)

var dir, _ = os.Getwd()
var pactDir = fmt.Sprintf("%s/../pacts", dir)
var logDir = fmt.Sprintf("%s/log", dir)

func TestPactProvider(t *testing.T) {
	go startProviderServer()

	pact := dsl.Pact{
		Provider:                 "todoserver",
		LogDir:                   logDir,
		PactDir:                  pactDir,
		DisableToolValidityCheck: true,
		LogLevel:                 "INFO",
	}

	_, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            "http://localhost:5000",
		FailIfNoPactsFound:         true,
		BrokerURL:                  os.Getenv("PACT_URL"),
		BrokerUsername:             os.Getenv("PACT_USERNAME"),
		BrokerPassword:             os.Getenv("PACT_PASSWORD"),
		PublishVerificationResults: true,
		ProviderVersion:            os.Getenv("PROVIDER_VERSION"),
	})
	if err != nil {
		t.Error(err)
	}
}

func startProviderServer() {
	log.Printf("API starting: port 5000")
	database.SetupTestDatabase()
	todo := models.Todo{ID: 1, Text: "Todo Item 1"}
	database.DB.Create(&todo)

	app := fiber.New()
	routers.SetupRoutes(app)

	log.Fatal(app.Listen(":5000"))
}
